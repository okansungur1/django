# Django 
Django is one of the most popular Python web framework.Django provides a complete mechanism for managing database,templates,rest. We can define Django as a full-stack web framework, whereas Flask is a light-weight (micro) framework when compared.Although Django is a production-ready framework if the requirements change dynamically it is not preffered. Django is good for big projects whereas Flask for simple applications that dont require too much coding.
The Django has MVT(Model View Template) pattern 
- Model: Manages data and business.(Data Access Layer)
- View: Deals with the business logic and returns a response to the user by the help of a model to carry data to templates
- Presentation: Html, css, javascript and static files (presentation layer)

 Virtual environments provide managing project dependencies.**pip** is a package manager. Allows to install python packages easily.
 
A Django project contains packages and files
- **manage.py** : A command-line utility that executes Django-specific tasks
- **_init__.py**: An empty file that tells Python to treat directories as a Python package
- **settings.py**: Django application configurations are kept here.
- **urls.py**: Contain listed URLs for the web application
- **wsgi.py**:(Web Server Gateway Interface)Entry-point to serve Django project.WSGI is a standard that defines how applications and servers communicate with each other.

By default settings file configures  SQLite database.INSTALLED_APPS we can configure projects helper libraries. With TEMPLATES we can create our html files. With STATIC_URL we can define our static files such as images css and javacscript files.Django uses migration files to keep app models and database in sync. One model class generally maps  to a single database table. After creating our models we need to apply them with the help of the commands
```
python manage.py makemigrations
python manage.py migrate

```


![Python](img/python.png "Python")
Python

After downloading python. Check the python version from the commnand line.We will also need to install Django and Djangorest framework.


![Django](img/django.png "Django")
Django



![Rest](img/djangorest.png "Rest")

We will use PyCharm as the IDE as it provides lots of features for developers.



![PyCharm](img/project1.png "PyCharm")
PyCharm



Create a project WebApp.



![My_Web_Project](img/myfirstapp.png "My_Web_Project")
My_Web_Project

![Output of the web application](img/myfirstapp3.png "Output of the web application")

To keep code simple and clean  Django project vcan consist multiple applications. To create an application we will use the command __manage.py startup my_web_project__ command.






![My_Web_Project](img/createmy_web_proj.png "My_Web_Project")


We will use a database SQLite,  where  we define in __settings.py__.
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'MyDatabase'),
    }
}
```
 Then  We will create our tables by using __models.py__
 ```
class Students(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    age = models.IntegerField(null=True)
```



![Embeded Database](img/database.png "Embeded Database")
Embeded Database

The pip freeze > requirements.txt command produces a list of all installed packages.This will be used in docker file.
With migration command we execute  SQL commands in the database file and all the tables of  installed apps are created in our database. You can check the migration folder after executing the command.


![Migration](img/django3.png "Migration")
Migration

To add an initial data we added migrations.RunSQL command to our generated code __0001_initial.py__.




![myfirstapp](img/myfirstapp2.png "myfirstapp")





To dockerize the application We have to make some modifications. We changed ALLOWED_HOSTS parameter from the __settings.py__.
And we prepare a requirements.txt. This file  lists all of the modules needed for the Django project to work.




![Hosts Configuration](img/dockerize1.png "Hosts Configuration")

Dockerize the web application
```
docker run -p 8000:8000 greenredblue/mydjango:v1
```



![Web Project Output](img/output.png "Web Project Output")

Test the rest post method with Postman


![Testing post method](img/PostMethod.png "Testing post method")

